# Label To Id

Just an example of copying Inkscape labels to svg ids.

Inkscape may refuse to copy if the object id already exist.

Would  recommend working on a copy of your document. 

*These changes are irreversible.*

Changing ids in this way can break some svg elements which use referencing.

For example - <use> elements - if the id of the object they point to is changed.

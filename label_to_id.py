import inkex

class LabelToId(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--selection_type", type=str, dest="selection_type", default='selected_objects')
        pars.add_argument("--groups_bool", type=inkex.Boolean, dest="groups_bool", default=False)

    def effect(self):

        if self.options.selection_type == 'selected':
            selection_list = self.svg.selected

        else:
            # Select all drawable items in document
            selection_list = [item for item in self.svg.iterdescendants() if isinstance(item, (inkex.ShapeElement))]

        # Remove any groups if user desires

        if not self.options.groups_bool:
            for item in selection_list:
                if item.TAG == 'g':
                    if item.get('inkscape:groupmode') != 'layer':
                        selection_list.remove(item)

        # Remove any layers in the selection

        for item in selection_list:
            if item.TAG == 'g':
                if item.get('inkscape:groupmode') == 'layer':
                    selection_list.remove(item)

        # Exit if nothing do to !

        if len(selection_list) < 1:
            inkex.errormsg('Nothing Selected')

        # Make the ID the label if label is present
        # Inkscape refuses to change
        # If another object in document already has that id

        for element in selection_list:
            element_label = element.get('inkscape:label')
            if element_label != None:
                element.set('id', element_label)


if __name__ == '__main__':
    LabelToId().run()
